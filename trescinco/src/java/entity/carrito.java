
package entity;


public class carrito 
{
    Maquinaria maquinaria;
    int cantidad=0;

    public carrito(Maquinaria mascota, int cantidad) {
        this.maquinaria = mascota;
        this.cantidad = cantidad;
    }

    public Maquinaria getMasquinaria() {
        return maquinaria;
    }

    public void setMascota(Maquinaria maquinaria) {
        this.maquinaria = maquinaria;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    
}
