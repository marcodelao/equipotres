package ui.entity;

import entity.Entregas;
import dal.facade.EntregasFacade;
import ui.bean.util.JsfUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

@FacesConverter(value = "entregasConverter")
public class EntregasConverter implements Converter {

    @Inject
    private EntregasFacade ejbFacade;

    private static final String SEPARATOR = "#";
    private static final String SEPARATOR_ESCAPED = "\\#";

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.length() == 0 || JsfUtil.isDummySelectItem(component, value)) {
            return null;
        }
        return this.ejbFacade.find(getKey(value));
    }

    entity.EntregasPK getKey(String value) {
        entity.EntregasPK key;
        String values[] = value.split(SEPARATOR_ESCAPED);
        key = new entity.EntregasPK();
        key.setId(Integer.parseInt(values[0]));
        key.setVenta(Integer.parseInt(values[1]));
        return key;
    }

    String getStringKey(entity.EntregasPK value) {
        StringBuffer sb = new StringBuffer();
        sb.append(value.getId());
        sb.append(SEPARATOR);
        sb.append(value.getVenta());
        return sb.toString();
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        if (object == null
                || (object instanceof String && ((String) object).length() == 0)) {
            return null;
        }
        if (object instanceof Entregas) {
            Entregas o = (Entregas) object;
            return getStringKey(o.getEntregasPK());
        } else {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Entregas.class.getName()});
            return null;
        }
    }

}
