/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.bean;

import entity.Maquinaria;
import entity.carrito;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean(name="carritoItem")
@RequestScoped
public class carritoItem {
    
    private carrito carrito;
    private final Double iva=.16;
    private Double subtotal=0.0;
    private Double total=0.0;
    private int cantidad=0;
    @ManagedProperty(value="#{mapeoCarrito}")    
    private carritoInterface carritoInterface;
        
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    private Maquinaria maquina;

    public Maquinaria getMaquina() {
        return maquina;
    }

    public void setMaquina(Maquinaria maquina) {
        this.maquina = maquina;
    }
    
    
    
    public int getCantidad() {
        return cantidad;
    }

    public carrito getCarrito() {
        return carrito;
    }

    public void setCarrito(carrito carrito) {
        this.carrito = carrito;
    }
    
    

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    

    


    public Double getSubtotal() {      
        totSub();
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getTotal() {      
        totSub();
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public carritoInterface getCarritoInterface() {
        return carritoInterface;
    }

    public void setCarritoInterface(carritoInterface carritoInterface) {
        this.carritoInterface = carritoInterface;
    }
    

    /**
     * Creates a new instance of carritoItem
     */
    public carritoItem() {
    }
    
    public void totSub()
    {
        subtotal=0.0;
        total=0.0;
        ArrayList lis=(ArrayList) listar();
        
        for (int i = 0; i < lis.size(); i++) 
        {
            carrito tem=(carrito) lis.get(i);
           // BigDecimal precio=tem.getMascota().getPrecio();
            subtotal=subtotal+(tem.getCantidad()*tem.getMasquinaria().getPrecio());
        }
        total=subtotal;
        cantidad=0;
    }
    
    public void alta()
    {
        System.out.println("");
        carrito car=new carrito(this.maquina, 1);
        carritoInterface.addCarrito(car);
        totSub();
        
    }
    
    
    public String register(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("You've Registered In:", this.maquina.getNombre()));
        return "";
    }
    
    public void baja(carrito car)
    {
        totSub();
        carritoInterface.eliminaCarrito(car);
    }
    
    Collection<carrito> lista;
    
    public Collection<carrito> listar()
    {
        
        return carritoInterface.ListaCuates();
    }

    public Collection<carrito> getLista() {
        return listar();
    }

    public void setLista(Collection<carrito> lista) {
        this.lista = lista;
    }
    
    public String limpiar()
    {
        System.out.println("");
        total=0.0;
        subtotal=0.0;        
        carritoInterface.eliminaTodo();

        return "index";
        
    }
    
    
}
