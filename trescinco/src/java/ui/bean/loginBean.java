/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.bean;

import dal.facade.SucursalFacade;
import dal.facade.UsuarioFacade;
import entity.Sucursal;
import entity.Usuario;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@ManagedBean
@SessionScoped
public class loginBean implements Serializable{
    @NotNull           
    @Size(min=1,max=30)
    String usuario;
    @NotNull
    @Size(min=1,max=30)
    String password;
    Usuario us;
    String sucursal;
    @EJB
    private UsuarioFacade usuarioFacade;
    @EJB
    private SucursalFacade sucursalFacade;
    
    /**
     * Creates a new instance of loginBean
     */
    public loginBean() {
    }
    
    public void saveMessage() throws IOException {
        
      FacesContext context = FacesContext.getCurrentInstance();
        System.out.println("");
      boolean a=usuarioFacade.validaEntrada(usuario, password);
        if (a) 
        {
            HttpSession session = (HttpSession) context.getExternalContext().getSession(false );
            Usuario usa=usuarioFacade.usuario(usuario);
            
            Sucursal suc=sucursalFacade.buscaSucursal(usa.getBodega().getId()+"");
            
            setSucursal(suc.getId()+"");            
            context.addMessage(null, new FacesMessage("Usuario existe",  "Your message: ") );
            context.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));            
            context.getExternalContext().redirect("/trescinco/faces/ventas.xhtml");
            
        }
                                                                                                      
    }
    
    
    
    

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Usuario getUs() {
        return us;
    }

    public void setUs(Usuario us) {
        this.us = us;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }
    
    
    
}
