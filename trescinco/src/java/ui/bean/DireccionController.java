package ui.bean;

import entity.Direccion;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "direccionController")
@ViewScoped
public class DireccionController extends AbstractController<Direccion> {

    @Inject
    private ColoniaController coloniaController;

    public DireccionController() {
        // Inform the Abstract parent controller of the concrete Direccion Entity
        super(Direccion.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        coloniaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Colonia controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareColonia(ActionEvent event) {
        if (this.getSelected() != null && coloniaController.getSelected() == null) {
            coloniaController.setSelected(this.getSelected().getColonia());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Clientes entities that
     * are retrieved from Direccion?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Clientes page
     */
    public String navigateClientesCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Clientes_items", this.getSelected().getClientesCollection());
        }
        return "/entity/clientes/index";
    }

}
