package ui.bean;

import entity.Estado;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "estadoController")
@ViewScoped
public class EstadoController extends AbstractController<Estado> {

    public EstadoController() {
        // Inform the Abstract parent controller of the concrete Estado Entity
        super(Estado.class);
    }

}
