package ui.bean;

import entity.Usuario;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "usuarioController")
@ViewScoped
public class UsuarioController extends AbstractController<Usuario> {

    @Inject
    private BodegaController bodegaController;

   
    
    
    public UsuarioController() {
        // Inform the Abstract parent controller of the concrete Usuario Entity
        super(Usuario.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        bodegaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Bodega controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareBodega(ActionEvent event) {
        if (this.getSelected() != null && bodegaController.getSelected() == null) {
            bodegaController.setSelected(this.getSelected().getBodega());
        }
    }

    public BodegaController getBodegaController() {
        return bodegaController;
    }

    public void setBodegaController(BodegaController bodegaController) {
        this.bodegaController = bodegaController;
    }

    

    
    
}
