package ui.bean;

import dal.facade.LineaventaFacade;
import dal.facade.SucursalFacade;
import dal.facade.VentaFacade;
import entity.Clientes;
import entity.Lineaventa;
import entity.Sucursal;
import entity.Venta;
import entity.carrito;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "ventaController")
@ViewScoped
public class VentaController extends AbstractController<Venta> {

    @Inject
    private SucursalController sucursalController;
    @Inject
    private ClientesController clientesController;
    
    @EJB
    private VentaFacade ventaFacade;
    @EJB
    private SucursalFacade sucursalFacade;
    @EJB
    private LineaventaFacade lineaVentaFacade;

    public VentaController() {
        // Inform the Abstract parent controller of the concrete Venta Entity
        super(Venta.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        sucursalController.setSelected(null);
        clientesController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Lineaventa entities that
     * are retrieved from Venta?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Lineaventa page
     */
    public String navigateLineaventaCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Lineaventa_items", this.getSelected().getLineaventaCollection());
        }
        return "/entity/lineaventa/index";
    }

    /**
     * Sets the "selected" attribute of the Sucursal controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSucursal(ActionEvent event) {
        if (this.getSelected() != null && sucursalController.getSelected() == null) {
            sucursalController.setSelected(this.getSelected().getSucursal());
        }
    }

    /**
     * Sets the "selected" attribute of the Clientes controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareClientes(ActionEvent event) {
        if (this.getSelected() != null && clientesController.getSelected() == null) {
            clientesController.setSelected(this.getSelected().getClientes());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Entregas entities that
     * are retrieved from Venta?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Entregas page
     */
    public String navigateEntregasCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Entregas_items", this.getSelected().getEntregasCollection());
        }
        return "/entity/entregas/index";
    }
    
    
    @PostConstruct
    public void fooInit(){
        a=new Venta();
    }
    Venta a;
    
     
    public void regitraVenta(Clientes cliente,String sucursal,Collection<carrito> carrito) throws IOException
    {
        System.out.println("");        
        Sucursal suc=sucursalFacade.buscaSucursal(sucursal);
        
        Calendar fecha = new GregorianCalendar();       
        int año = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        a.setClientes(cliente);        
        a.setFechaestimada(new Date(año, mes, dia+4));
        a.setFechaventa(new Date());
        
        a.setSucursal(suc);
        
        //a.setLineaventaCollection(null);
        ventaFacade.create(a);
        
        System.out.println("");
        ArrayList<carrito> arreglo = (ArrayList<carrito>) carrito;
        
        for (carrito arreglo1 : arreglo) {
            Lineaventa lV=new Lineaventa();
            lV.setCantidad(arreglo1.getCantidad());
            lV.setMaquinaria(arreglo1.getMasquinaria());
            lV.setVenta(a);
            lineaVentaFacade.create(lV);
        }
        carrito.clear();
        
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Venta","Fecha de venta"+a.getFechaventa().toString()+"\n"
                + "RFC del cliente"+a.getClientes().getRfc()+"\n"+"Nombre de la sucursal"+a.getSucursal().getNombre()
        +"\n"+"") );
        context.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));  
        //context.getExternalContext().redirect("/trescinco/faces/ventas.xhtml");
        
    }

}
