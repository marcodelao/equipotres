/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.bean;


import entity.carrito;
import java.util.ArrayList;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;


@ManagedBean(name="mapeoCarrito")
@ApplicationScoped
public class mapeoCarrito implements carritoInterface{

    private ArrayList<carrito> lista;

    public mapeoCarrito() 
    {
        lista= new ArrayList<>();
    }
    
    @Override
    public void addCarrito(carrito carrito) {
        lista.add(carrito);
    }

    @Override
    public ArrayList<carrito> ListaCuates() {
        return lista;
    }

    @Override
    public void eliminaCarrito(carrito carrito) {
        lista.remove(carrito);
    }

    @Override
    public void eliminaTodo() {
        lista.clear();
    }
    
}
