/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.bean;

import dal.facade.UsuarioFacade;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@ManagedBean
@SessionScoped
@FacesValidator("validacionController")
public class validacionController  implements Validator{

    @EJB
    private UsuarioFacade usuarioFacade;
    /**
     * Creates a new instance of validacionController
     */
    public validacionController() {
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
              
        System.out.println("");
        if (usuarioFacade.validaUsuario(value.toString())==false) {
            FacesMessage msg = new FacesMessage(
                    " No existe el usuario");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
 
            throw new ValidatorException(msg);
        }
    }
    
    
     
    
}
