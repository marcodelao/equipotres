package ui.bean;

import entity.Bodega;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "bodegaController")
@ViewScoped
public class BodegaController extends AbstractController<Bodega> {

    public BodegaController() {
        // Inform the Abstract parent controller of the concrete Bodega Entity
        super(Bodega.class);
    }

    /**
     * Sets the "items" attribute with a collection of Sucursal entities that
     * are retrieved from Bodega?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Sucursal page
     */
    public String navigateSucursalCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Sucursal_items", this.getSelected().getSucursalCollection());
        }
        return "/entity/sucursal/index";
    }

    /**
     * Sets the "items" attribute with a collection of Usuario entities that are
     * retrieved from Bodega?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Usuario page
     */
    public String navigateUsuarioCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Usuario_items", this.getSelected().getUsuarioCollection());
        }
        return "/entity/usuario/index";
    }

    /**
     * Sets the "items" attribute with a collection of Camion entities that are
     * retrieved from Bodega?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Camion page
     */
    public String navigateCamionCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Camion_items", this.getSelected().getCamionCollection());
        }
        return "/entity/camion/index";
    }

}
