package ui.bean;

import entity.Sucursal;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "sucursalController")
@ViewScoped
public class SucursalController extends AbstractController<Sucursal> {

    @Inject
    private BodegaController bodegaController;

    public SucursalController() {
        // Inform the Abstract parent controller of the concrete Sucursal Entity
        super(Sucursal.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        bodegaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Bodega controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareBodega(ActionEvent event) {
        if (this.getSelected() != null && bodegaController.getSelected() == null) {
            bodegaController.setSelected(this.getSelected().getBodega());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Venta entities that are
     * retrieved from Sucursal?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Venta page
     */
    public String navigateVentaCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Venta_items", this.getSelected().getVentaCollection());
        }
        return "/entity/venta/index";
    }

}
