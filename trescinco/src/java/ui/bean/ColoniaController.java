package ui.bean;

import entity.Colonia;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "coloniaController")
@ViewScoped
public class ColoniaController extends AbstractController<Colonia> {

    public ColoniaController() {
        // Inform the Abstract parent controller of the concrete Colonia Entity
        super(Colonia.class);
    }

    /**
     * Sets the "items" attribute with a collection of Direccion entities that
     * are retrieved from Colonia?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Direccion page
     */
    public String navigateDireccionCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Direccion_items", this.getSelected().getDireccionCollection());
        }
        return "/entity/direccion/index";
    }

}
