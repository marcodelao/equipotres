package ui.bean;

import entity.Camion;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "camionController")
@ViewScoped
public class CamionController extends AbstractController<Camion> {

    @Inject
    private BodegaController bodegaController;

    public CamionController() {
        // Inform the Abstract parent controller of the concrete Camion Entity
        super(Camion.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        bodegaController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Entregas entities that
     * are retrieved from Camion?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Entregas page
     */
    public String navigateEntregasCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Entregas_items", this.getSelected().getEntregasCollection());
        }
        return "/entity/entregas/index";
    }

    /**
     * Sets the "selected" attribute of the Bodega controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareBodega(ActionEvent event) {
        if (this.getSelected() != null && bodegaController.getSelected() == null) {
            bodegaController.setSelected(this.getSelected().getBodega());
        }
    }
}
