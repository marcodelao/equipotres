package ui.bean;

import dal.facade.ClientesFacade;
import entity.Clientes;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "clientesController")
@ViewScoped
public class ClientesController extends AbstractController<Clientes> {

    @Inject
    private DireccionController direccionController;

    @EJB
    private ClientesFacade clienteFacade;
    
    
    private String rfc;    
    
    public ClientesController() {
        // Inform the Abstract parent controller of the concrete Clientes Entity
        super(Clientes.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        direccionController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Venta entities that are
     * retrieved from Clientes?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Venta page
     */
    public String navigateVentaCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Venta_items", this.getSelected().getVentaCollection());
        }
        return "/entity/venta/index";
    }

    /**
     * Sets the "selected" attribute of the Direccion controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareDireccion(ActionEvent event) {
        if (this.getSelected() != null && direccionController.getSelected() == null) {
            direccionController.setSelected(this.getSelected().getDireccion());
        }
    }
    @PostConstruct
    public void fooInit(){
        a=new Clientes();
    }
    Clientes a;
    
    public void buscaCliente() throws IOException {
        
      FacesContext context = FacesContext.getCurrentInstance();
      System.out.println("");
      a=clienteFacade.buscaCliente(rfc);
        if (a==null) 
        {            
            context.addMessage(null, new FacesMessage("Cliente no existe",  "Your message: ") );            
            
        }else
        {
            context.addMessage(null, new FacesMessage("Cliente  existe",  "Your message: ") );            
        }
                                                                                                      
    }
    
    public void guardar() throws IOException
    {
        System.out.println("h");
        clienteFacade.create(a);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Usuario Registrado",  "Your message: ") );        
        context.getExternalContext().redirect("/trescinco/faces/buscaCliente.xhtml");
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public Clientes getA() {
        return a;
    }

    public void setA(Clientes a) {
        this.a = a;
    }
    
    
}
