package ui.bean;

import entity.Lineaventa;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "lineaventaController")
@ViewScoped
public class LineaventaController extends AbstractController<Lineaventa> {

    @Inject
    private VentaController ventaController;
    @Inject
    private MaquinariaController maquinariaController;

    public LineaventaController() {
        // Inform the Abstract parent controller of the concrete Lineaventa Entity
        super(Lineaventa.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        ventaController.setSelected(null);
        maquinariaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Venta controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareVenta(ActionEvent event) {
        if (this.getSelected() != null && ventaController.getSelected() == null) {
            ventaController.setSelected(this.getSelected().getVenta());
        }
    }

    /**
     * Sets the "selected" attribute of the Maquinaria controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareMaquinaria(ActionEvent event) {
        if (this.getSelected() != null && maquinariaController.getSelected() == null) {
            maquinariaController.setSelected(this.getSelected().getMaquinaria());
        }
    }
}
