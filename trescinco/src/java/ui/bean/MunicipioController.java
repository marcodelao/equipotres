package ui.bean;

import entity.Municipio;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "municipioController")
@ViewScoped
public class MunicipioController extends AbstractController<Municipio> {

    public MunicipioController() {
        // Inform the Abstract parent controller of the concrete Municipio Entity
        super(Municipio.class);
    }

}
