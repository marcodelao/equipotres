package ui.bean;

import entity.Entregas;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "entregasController")
@ViewScoped
public class EntregasController extends AbstractController<Entregas> {

    @Inject
    private VentaController venta1Controller;
    @Inject
    private CamionController camionController;

    public EntregasController() {
        // Inform the Abstract parent controller of the concrete Entregas Entity
        super(Entregas.class);
    }

    @Override
    protected void setEmbeddableKeys() {
        this.getSelected().getEntregasPK().setVenta(this.getSelected().getVenta1().getId());
    }

    @Override
    protected void initializeEmbeddableKey() {
        this.getSelected().setEntregasPK(new entity.EntregasPK());
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        venta1Controller.setSelected(null);
        camionController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Venta controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareVenta1(ActionEvent event) {
        if (this.getSelected() != null && venta1Controller.getSelected() == null) {
            venta1Controller.setSelected(this.getSelected().getVenta1());
        }
    }

    /**
     * Sets the "selected" attribute of the Camion controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareCamion(ActionEvent event) {
        if (this.getSelected() != null && camionController.getSelected() == null) {
            camionController.setSelected(this.getSelected().getCamion());
        }
    }
}
