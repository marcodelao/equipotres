package ui.bean;

import entity.Maquinaria;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "maquinariaController")
@ViewScoped
public class MaquinariaController extends AbstractController<Maquinaria> {

    public MaquinariaController() {
        // Inform the Abstract parent controller of the concrete Maquinaria Entity
        super(Maquinaria.class);
    }

    /**
     * Sets the "items" attribute with a collection of Lineaventa entities that
     * are retrieved from Maquinaria?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Lineaventa page
     */
    public String navigateLineaventaCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Lineaventa_items", this.getSelected().getLineaventaCollection());
        }
        return "/entity/lineaventa/index";
    }

}
