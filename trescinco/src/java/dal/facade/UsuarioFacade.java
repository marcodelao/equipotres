/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal.facade;

import entity.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "trescincoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public boolean validaEntrada(String us,String pwd)
    {
        System.out.println("hola");
      Query tst=em.createQuery("select u from Usuario u where u.login=:login and u.pwd=:pwd");
      tst.setParameter("login", us);
      tst.setParameter("pwd", pwd);
      List<Usuario> usu=tst.getResultList();
        if (usu.isEmpty()) {
            return false;
        }else
        {
            return true;
        }          
    }
    
    public Usuario usuario(String us)
    {
        System.out.println("hola");
      Query tst=em.createQuery("select u from Usuario u where u.login=:login");
      tst.setParameter("login", us);      
      List<Usuario> usu=tst.getResultList();
        if (usu.isEmpty()) {
            return null;
        }else
        {
            return usu.get(0);
        }          
    }
    
    
    public boolean validaUsuario(String us)
    {     
        System.out.println("");
        System.out.println("");
      Query tst=em.createQuery("select u from Usuario u where u.login=:login");
      tst.setParameter("login", us);      
      List<Usuario> usu=tst.getResultList();
        if (usu.isEmpty()) {
            return false;
        }else
        {
            return true;
        }          
    }
    
}
