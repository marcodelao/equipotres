/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal.facade;

import entity.Clientes;
import entity.Sucursal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class SucursalFacade extends AbstractFacade<Sucursal> {
    @PersistenceContext(unitName = "trescincoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SucursalFacade() {
        super(Sucursal.class);
    }
    
    
    public Sucursal buscaSucursal(String id)
    {     
        System.out.println("");
        System.out.println("");
      Query tst=em.createQuery("select u from Sucursal u where u.bodega.id=:id");
      tst.setParameter("id", Integer.parseInt(id));      
      List<Sucursal> usu=tst.getResultList();
        if (usu.isEmpty()) {
            return null;
        }else
        {
            return usu.get(0);
        }          
    }
}
