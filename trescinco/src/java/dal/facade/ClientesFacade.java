/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal.facade;

import entity.Clientes;
import entity.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class ClientesFacade extends AbstractFacade<Clientes> {
    @PersistenceContext(unitName = "trescincoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClientesFacade() {
        super(Clientes.class);
    }
    
    public Clientes buscaCliente(String rfc)
    {     
        System.out.println("");
        System.out.println("");
      Query tst=em.createQuery("select u from Clientes u where u.rfc=:rfc");
      tst.setParameter("rfc", rfc);      
      List<Clientes> usu=tst.getResultList();
        if (usu.isEmpty()) {
            return null;
        }else
        {
            return usu.get(0);
        }          
    }
    
}
